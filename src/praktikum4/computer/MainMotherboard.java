package computer;
import java.util.Scanner;

public class MainMotherboard {
	public static void main(String[] args) {
		Scanner theinput = new Scanner(System.in);
		int usb3 = theinput.nextInt();
		int usb2 = theinput.nextInt();
		MotherBoard.USB theUSB = new MotherBoard.USB(); 
		int total = theUSB.getTotalPorts(usb3, usb2);
		System.out.println("Total Ports = " + total);
	}
}
