package week2;

public class AksiLampu{
	public static void main(String[] args) {
		Lampu lampuku = new Lampu();
		lampuku.matikanLampu();
		
		System.out.println("Apakah Lampu Menyala? " + lampuku.nyala);
	}
}