package week2;

public class AksiSepeda {
	
	public static void main(String[] args) {
		// membuat objek
		Sepeda sepedaBalap = new Sepeda(2, "Sepeda Kaki","Andromeda");
		Sepeda sepedaAnak = new Sepeda(2, "Sepeda Matic","Uranus");
			
		int gearSepeda = sepedaBalap.gear;
		System.out.println(gearSepeda);
		sepedaBalap.ngerem();
		
		int anakGearSepeda = sepedaAnak.gear;
		System.out.println(anakGearSepeda);
		sepedaAnak.ngerem();
	}
	
}
