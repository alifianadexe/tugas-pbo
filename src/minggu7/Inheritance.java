

import java.util.Scanner;

class Person{
    protected String nama;
    protected String alamat;
    protected String kegemaran;

    public void identity(){
        System.out.println("Nama: " + nama);
        System.out.println("Alamat: " + alamat);
        System.out.println("Hobi: " + kegemaran);
    }
}

class Student extends Person{
    String nim;
    Integer sks, modul, spp;

    public void identity(){
        super.identity();
        System.out.println("NIM: " + nim);
    }

    public void hitungPembayaran(){
        Integer result = sks + modul + spp;
        System.out.println("Total tagihan pembayaran mahasiswa: " + result);
    }
}

public class Inheritance{
    public static void main(String[] args){
        Student mahasiswa = new Student();
        Scanner input = new Scanner(System.in);

        System.out.print("Nama: "); mahasiswa.nama = input.nextLine();
        System.out.print("Alamat: "); mahasiswa.alamat = input.nextLine();
        System.out.print("Hobi: "); mahasiswa.kegemaran = input.nextLine();
        System.out.print("NIM: "); mahasiswa.nim = input.nextLine();
        System.out.print("Tagihan SKS: "); mahasiswa.sks = input.nextInt();
        System.out.print("Tagihan Modul: "); mahasiswa.modul = input.nextInt();
        System.out.print("Tagihan spp: "); mahasiswa.spp = input.nextInt();
        System.out.println("\nData Mahasiswa: ");

        mahasiswa.identity();
        mahasiswa.hitungPembayaran();

        input.close();
    }
}
